# Big Brother's eye

## Purpose

I don't trust Big Brother to respect the do not track request from user. Therefore this small bit of code checks the value and inject or not the analytics library.

## Usage

In your html pages:
```html
<script async src="./big_brother_eye.js"></script>
<script>
    window.big_brother = {
        'key' : 'CHANGE_ME'
    }
</script>
```
Notes:
- Replace the `CHANGE_ME` value by your analytics tag/key.
- You might want to review the above path.
