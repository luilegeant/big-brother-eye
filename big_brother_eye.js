/**
 * Big Brother's eye
 * Author: Luilegeant
 * License: AGPLv3.0 https://www.gnu.org/licenses/agpl-3.0.en.html
 */
window.onload = function() {
	if(navigator.doNotTrack) {
		console.log("No tracking ?!");
		console.log("Alright...");
		console.log("I'm not injecting Big Brother's eye here.");
		console.log("I like you ! ! !");
	} else {
		if(window.big_brother && window.big_brother.key) {
			var gAnalyticsJSLibraryNode = document.createElement('script');
			gAnalyticsJSLibraryNode.type = 'text/javascript';
			gAnalyticsJSLibraryNode.async = true;
			gAnalyticsJSLibraryNode.src = "https://www.googletagmanager.com/gtag/js?id=" + window.big_brother.key; 
			document.body.appendChild(gAnalyticsJSLibraryNode);
			var gAnalyticsBootstrapper = document.createElement('script');
			gAnalyticsBootstrapper.type = 'text/javascript';
			var gConfig = 'window.dataLayer = window.dataLayer || [];'+
						'function gTag(){dataLayer.push(arguments);}'+
						'gTag(\'js\', new Date());'+
						'gTag(\'config\', \'' + window.big_brother.key + '\');';
			try {
				gAnalyticsBootstrapper.appendChild(document.createTextNode(gConfig));
			} catch (e) {
				gAnalyticsBootstrapper.text = gConfig;
			}
			document.body.appendChild(gAnalyticsBootstrapper);
			console.log("                    ..                           ");
			console.log("          `OOOOOOOOOOOOOOO/,.                    ");
			console.log("         .OOOOOOOOOOOOOOOOOOOOOO].               ");
			console.log("          .OOOOOOOOOOOOOOOOOOOOOOOOOO],.         ");
			console.log("           .OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\\,.    ");
			console.log("           `oOOOOOOOOOOOOOOOOOOOOO@@@@@@@@@@@@@@.");
			console.log("             `\\oOOOOOOOOOOOOO@@@@@@@@@@@@@@O@@@@.");
			console.log("                \\oOOOOOOOOOO@@@@@       @@O@@@@= ");
			console.log("     .            `oOOOOOOO@@@@@  ( 0    @O@@@@. ");
			console.log(" ^@@@@@@            .OoOOO@O@@@@@       @OO/@@,  ");
			console.log(" ^@@@@@@=            .@@@ooOO@@@@@@@@@@@@@O.     ");
			console.log(" ^@@@@@@@@O]]]]]]. ..@@@@@OOOOOOO[[[[[[[..       ");
			console.log(" ^@@@@@@@@@@@@@@@@@@@@@@@@@.                     ");
			console.log(" ^@@@@@@@@@@@@@@@@@@@@@@@@=                      ");
			console.log(" ^@@@@@@.            `[[[,                       ");
			console.log(" ^@@@@[,                                         ");
			console.log("                                                 ");
			console.log("It's not because we don't have anything to hide that we should have no privacy !");
			console.log("Change your configuration and request to not be tracked in your browser's settings.");
		} else {
			console.error("Big Brother's eye: You might have forgotten to set the google analytics id to this attribute: 'window.big_brother.key'");
		}
	}
}
